/* eslint-disable react-hooks/rules-of-hooks */
"use client";
import React from "react";
import Navbar from "@/components/Navbar";
import { format } from "date-fns";
import { useState, useEffect } from "react";
import { SessionProvider, getSession } from "next-auth/react";
import { useRouter } from "next/navigation";

const monitoringPage = () => {
  const [loading, setLoading] = useState(true);
  const [session, setSession] = useState(null);
  const router = useRouter();

  useEffect(() => {
    const checkSession = async () => {
      const session = await getSession();
      if (!session) {
        router.push("/");
      } else {
        setSession(session);
        setLoading(false);
      }
    };
    checkSession();
  }, [router]);

  if (loading) {
    return (
      <div className="flex items-center justify-center min-h-screen">
        <div className="text-center text-white text-4xl">LOADING...</div>
      </div>
    );
  }

  const currentDate = new Date();
  const dateTime = format(currentDate, "EEEE, d MMMM yyyy");

  return (
    <>
      <SessionProvider>
        <Navbar />
        <div className="ml-10 mt-10 flex flex-col px-20 font-custom">
          <div className="mb-6">
            <div className="flex gap-2 items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="50px"
                viewBox="0 -960 960 960"
                width="50px"
                fill="#e8eaed"
              >
                <path d="M120-120v-80l80-80v160h-80Zm160 0v-240l80-80v320h-80Zm160 0v-320l80 81v239h-80Zm160 0v-239l80-80v319h-80Zm160 0v-400l80-80v480h-80ZM120-327v-113l280-280 160 160 280-280v113L560-447 400-607 120-327Z" />
              </svg>
              <div className="text-4xl text-white font-bold">Monitoring</div>
            </div>
            <div className="dateNow text-white font-bold text-xl">
              {dateTime}
            </div>
          </div>
          <div className="youtubelist text-white">INI MONITORING</div>
        </div>
      </SessionProvider>
    </>
  );
};

export default monitoringPage;
