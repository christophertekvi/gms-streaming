"use client";
import React from "react";
import { useState } from "react";
import { signIn } from "next-auth/react";
import Image from "next/image";

const Home = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const result = await signIn("credentials", {
      redirect: false,
      username,
      password,
    });

    if (result?.error) {
      alert("Invalid credentials");
    } else {
      alert("Logged in successfully");
      window.location.href = "/dashboard";
    }
  };

  return (
    <>
      <div className="flex justify-center items-center mt-20 font-custom">
        <div className="place-items-center flex flex-col justify-center items-center">
          <div className="logo mb-8">
            <Image width={500} height={150} src="/LOGO.png" alt="LOGO" />
          </div>
          <div className="container-login rounded-3xl shadow-2xl bg-[#134074] w-full max-w-5xl px-10 py-y4 sm:max-w-xl sm:px-6">
            <form onSubmit={handleSubmit}>
              <h1 className="text-white font-bold text-3xl pt-8 sm:text-2xl">
                Login
              </h1>
              <div className="username my-2">
                <label htmlFor="username" className="text-white font-bold">
                  Username
                </label>
                <input
                  type="text"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  required
                  placeholder="Enter your username"
                  className="block mt-1 w-full p-2 rounded-lg"
                />
              </div>
              <div className="password">
                <label className="text-white font-bold">Password</label>
                <input
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                  placeholder="Enter your password"
                  className="mt-1 w-full p-2 rounded-lg"
                />
              </div>
              <div className="text-center my-6">
                <button
                  type="submit"
                  className="text-black bg-white font-bold rounded-lg px-12 py-3 text-xl hover:bg-slate-200"
                >
                  LOGIN
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
