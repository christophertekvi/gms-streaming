// app/dashboard/page.tsx
"use client";
import React, { useState, useEffect } from "react";
import Navbar from "@/components/Navbar";
import { format } from "date-fns";
import { SessionProvider, getSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import LivestreamSchedule from "@/components/LivestreamSchedule";
import { YoutubeSignIn } from "@/components/AuthButton";
import CompleteStreaming from "@/components/CompleteStreaming";

const DashboardPage = () => {
  const [loading, setLoading] = useState(true);
  const [session, setSession] = useState(null);
  const router = useRouter();

  useEffect(() => {
    const checkSession = async () => {
      const session = await getSession();
      if (!session) {
        router.push("/");
      } else {
        setSession(session);
        setLoading(false);
      }
    };
    checkSession();
  }, [router]);

  if (loading) {
    return (
      <div className="flex items-center justify-center min-h-screen">
        <div className="text-center text-white text-4xl">LOADING...</div>
      </div>
    );
  }

  const currentDate = new Date();
  const dateTime = format(currentDate, "EEEE, d MMMM yyyy");

  return (
    <>
      <SessionProvider>
        <Navbar />
        <div className="px-20 py-8">
          <div className="mb-4">
            <div className="flex gap-4 items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="50px"
                viewBox="0 -960 960 960"
                width="50px"
                fill="#e8eaed"
              >
                <path d="M180-120q-24 0-42-18t-18-42v-600q0-24 18-42t42-18h600q24 0 42 18t18 42v600q0 24-18 42t-42 18H180Zm0-60h270v-600H180v600Zm330 0h270v-301H510v301Zm0-361h270v-239H510v239Z" />
              </svg>
              <div className="text-4xl text-white font-bold">Dashboard</div>
            </div>
            <div className="dateNow text-white font-bold text-xl mt-4">
              {dateTime}
            </div>
          </div>
          {session?.accessToken ? (
            <>
              <div className="youtubelist">
                <LivestreamSchedule />
                <CompleteStreaming />
              </div>
            </>
          ) : (
            <YoutubeSignIn />
          )}
        </div>
      </SessionProvider>
    </>
  );
};

export default DashboardPage;
