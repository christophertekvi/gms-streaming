"use client";
import React, { useState, useEffect } from "react";
import Navbar from "@/components/Navbar";
import { SessionProvider, getSession } from "next-auth/react";
import { format } from "date-fns";
import { useRouter } from "next/navigation";

const SchedulePage = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [scheduledStartTime, setScheduledStartTime] = useState("");
  const [privacyStatus, setPrivacyStatus] = useState("public");
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [loading, setLoading] = useState(true);
  const [session, setSession] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const checkSession = async () => {
      const session = await getSession();
      if (!session) {
        router.push("/");
      } else {
        setSession(session);
        setLoading(false);
      }
    };
    checkSession();
  }, [router]);

  if (loading) {
    return (
      <div className="flex items-center justify-center min-h-screen">
        <div className="text-center text-white text-4xl">LOADING...</div>
      </div>
    );
  }

  const currentDate = new Date();
  const dateTime = format(currentDate, "EEEE, d MMMM yyyy");

  const handleDateTimeChange = (e) => {
    const value = e.target.value;
    setScheduledStartTime(value);

    // Convert the value to ISO 8601 format with UTC 'Z' suffix
    const isoDateTime = new Date(value).toISOString();
    console.log("ISO 8601 DateTime: ", isoDateTime); // You can use this value as needed
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);
    const isoDateTime = new Date(scheduledStartTime).toISOString();

    console.log(title, description, isoDateTime, privacyStatus);

    try {
      const response = await fetch("/api/scheduling", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          description,
          scheduledStartTime: isoDateTime,
          privacyStatus,
        }),
      });

      if (!response.ok) {
        throw new Error("Failed to schedule live broadcast");
      }

      const data = await response.json();
      setSuccess(data.message);
      setError(null);
    } catch (error) {
      setError(error.message);
      setSuccess(null);
    } finally {
      setIsSubmitting(false);
    }
  };

  if (status === "loading") {
    return (
      <div className="flex items-center justify-center min-h-screen">
        <div className="text-center text-white text-4xl">LOADING...</div>
      </div>
    );
  }

  return (
    <>
      <SessionProvider>
        <Navbar />
        <div className="px-20 py-8">
          <div className="mb-4">
            <div className="flex gap-4 items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="50px"
                viewBox="0 -960 960 960"
                width="50px"
                fill="#e8eaed"
              >
                <path d="M700-80v-120H580v-60h120v-120h60v120h120v60H760v120h-60Zm-520-80q-24 0-42-18t-18-42v-540q0-24 18-42t42-18h65v-60h65v60h260v-60h65v60h65q24 0 42 18t18 42v302q-15-2-30-2t-30 2v-112H180v350h320q0 15 3 30t8 30H180Zm0-470h520v-130H180v130Zm0 0v-130 130Z" />
              </svg>
              <div className="text-4xl text-white font-bold">Schedule</div>
            </div>
            <div className="dateNow text-white font-bold text-xl mt-4">
              {dateTime}
            </div>
          </div>
          <div className="formScheduling bg-[#134074] px-72 py-4 shadow-2xl rounded-2xl justify-center items-center flex-col">
            <form onSubmit={handleSubmit}>
              <div className="mb-4">
                <label
                  className="block text-white text-sm font-bold mb-2"
                  htmlFor="title"
                >
                  Title
                </label>
                <input
                  id="title"
                  type="text"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  className="shadow appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  required
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-white text-sm font-bold mb-2"
                  htmlFor="description"
                >
                  Description
                </label>
                <textarea
                  id="description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  className="shadow appearance-none border rounded w-full h-32 py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  required
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-white text-sm font-bold mb-2"
                  htmlFor="scheduledStartTime"
                >
                  Scheduled Start Time
                </label>
                <input
                  id="scheduledStartTime"
                  type="datetime-local"
                  value={scheduledStartTime}
                  onChange={handleDateTimeChange}
                  className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  required
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-white text-sm font-bold mb-2"
                  htmlFor="privacyStatus"
                >
                  Privacy Status
                </label>
                <select
                  id="privacyStatus"
                  value={privacyStatus}
                  onChange={(e) => setPrivacyStatus(e.target.value)}
                  className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  required
                >
                  <option value="public">Public</option>
                  <option value="unlisted">Unlisted</option>
                  <option value="private">Private</option>
                </select>
              </div>
              <div className="mb-4">
                <button
                  type="submit"
                  className={`bg-blue-500 hover:bg-blue-700 shadow-xl text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline ${
                    isSubmitting ? "cursor-not-allowed opacity-50" : ""
                  }`}
                  disabled={isSubmitting}
                >
                  {isSubmitting ? "Scheduling..." : "Schedule Live Stream"}
                </button>
              </div>
              {error && <div className="text-red-500 text-sm">{error}</div>}
              {success && (
                <div className="text-green-500 text-sm">{success}</div>
              )}
            </form>
          </div>
        </div>
      </SessionProvider>
    </>
  );
};

export default SchedulePage;
