import { NextRequest, NextResponse } from "next/server";
import { getToken } from "next-auth/jwt";
import axios from "axios";

// Function to refresh the access token
async function refreshAccessToken(refreshToken) {
  try {
    const response = await axios.post("https://oauth2.googleapis.com/token", {
      client_id: process.env.GOOGLE_CLIENT_ID,
      client_secret: process.env.GOOGLE_CLIENT_SECRET,
      refresh_token: refreshToken,
      grant_type: "refresh_token",
    });

    return response.data.access_token;
  } catch (error) {
    console.error("Failed to refresh access token:", error.response.data);
    throw new Error("Failed to refresh access token");
  }
}

// Function to fetch live broadcasts
export async function fetchLiveBroadcasts(token) {
  const baseUrl =
    "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=snippet,contentDetails,status";
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  const fetchBroadcasts = async (status, pageToken = "") => {
    const url = `${baseUrl}&broadcastStatus=${status}${
      pageToken ? `&pageToken=${pageToken}` : ""
    }`;
    const response = await fetch(url, {
      headers,
    });

    if (!response.ok) {
      throw new Error(
        `Error fetching ${status} broadcasts: ${response.statusText}`
      );
    }

    const data = await response.json();
    return data;
  };

  const fetchAllBroadcasts = async (status) => {
    let broadcasts = [];
    let pageToken = "";
    let moreResults = true;

    while (moreResults) {
      const data = await fetchBroadcasts(status, pageToken);
      broadcasts = broadcasts.concat(data.items);
      pageToken = data.nextPageToken || "";
      moreResults = Boolean(pageToken);
    }

    return broadcasts;
  };

  try {
    const [upcomingBroadcasts, activeBroadcasts] = await Promise.all([
      fetchAllBroadcasts("active"),
      fetchAllBroadcasts("upcoming"),
    ]);

    return [...upcomingBroadcasts, ...activeBroadcasts];
  } catch (error) {
    console.error("Error fetching data from YouTube API:", error);
    throw error;
  }
}

// Function to fetch channel details
export async function fetchChannelDetails(channelId, token) {
  const channelResponse = await fetch(
    `https://www.googleapis.com/youtube/v3/channels?part=snippet&id=${channelId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (!channelResponse.ok) {
    throw new Error(
      `Error fetching channel details: ${channelResponse.statusText}`
    );
  }

  const channelData = await channelResponse.json();
  return channelData.items[0]?.snippet?.title || "Unknown Channel";
}

// Function to fetch live status
export async function fetchLiveStatus(broadcastId, token) {
  const liveStatusResponse = await fetch(
    `https://www.googleapis.com/youtube/v3/liveBroadcasts?part=status&id=${broadcastId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (!liveStatusResponse.ok) {
    throw new Error(
      `Error fetching live status: ${liveStatusResponse.statusText}`
    );
  }

  const liveStatusData = await liveStatusResponse.json();
  const isStreaming =
    liveStatusData.items[0]?.status?.lifeCycleStatus === "live";
  const isEnd = liveStatusData.items[0]?.status?.lifeCycleStatus === "complete";
  return { isStreaming, isEnd };
}

// Main function to handle GET request
export async function GET(req) {
  let token = await getToken({ req, secret: process.env.NEXTAUTH_SECRET });

  if (!token || !token.accessToken) {
    console.log("No token or access token found:", token); // Debugging line
    return NextResponse.json({ error: "Unauthorized" }, { status: 401 });
  }

  try {
    // Check if the access token is expired and refresh it if necessary
    let accessToken = token.accessToken;
    if (Date.now() >= token.exp * 1000) {
      console.log("Access token expired, refreshing...");
      accessToken = await refreshAccessToken(token.refreshToken);
      // Update the token object with the new access token
      token.accessToken = accessToken;
    }

    console.log("Using access token:", accessToken); // Debugging line

    // Fetch live broadcasts
    const items = await fetchLiveBroadcasts(accessToken);

    // Extract relevant details for each broadcast
    const broadcasts = await Promise.all(
      items.map(async (item) => {
        const channelTitle = await fetchChannelDetails(
          item.snippet.channelId,
          accessToken
        );
        const { isStreaming, isEnd } = await fetchLiveStatus(
          item.id,
          accessToken
        );

        return {
          id: item.id,
          title: item.snippet.title,
          scheduledStartTime: item.snippet.scheduledStartTime,
          channelTitle,
          iframeSrc: `https://www.youtube.com/embed/${item.id}`,
          isStreaming,
          isEnd,
        };
      })
    );

    console.log("Fetched broadcasts:", broadcasts); // Debugging line
    return NextResponse.json(broadcasts, { status: 200 });
  } catch (error) {
    console.error("Error fetching data from YouTube API:", error); // Debugging line
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
