import { NextRequest, NextResponse } from "next/server";
import { getToken } from "next-auth/jwt";

// Function to fetch live broadcasts with pagination
async function fetchLiveBroadcasts(token: string) {
  const baseUrl =
    "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=snippet,contentDetails,status&broadcastStatus=completed";
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  const fetchPage = async (pageToken = "") => {
    const response = await fetch(
      `${baseUrl}${pageToken ? `&pageToken=${pageToken}` : ""}`,
      { headers }
    );

    if (!response.ok) {
      throw new Error(response.statusText);
    }

    return response.json();
  };

  let items = [];
  let pageToken = "";
  let moreResults = true;

  while (moreResults) {
    const data = await fetchPage(pageToken);
    items = items.concat(data.items);
    pageToken = data.nextPageToken || "";
    moreResults = Boolean(pageToken);
  }

  return items;
}

// Function to fetch channel details
async function fetchChannelDetails(channelId: string, token: string) {
  const response = await fetch(
    `https://www.googleapis.com/youtube/v3/channels?part=snippet&id=${channelId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  const data = await response.json();
  return data.items[0]?.snippet?.title || "Unknown Channel";
}

// Function to fetch live status
async function fetchLiveStatus(broadcastId: string, token: string) {
  const response = await fetch(
    `https://www.googleapis.com/youtube/v3/liveBroadcasts?part=status&id=${broadcastId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  const data = await response.json();
  const isStreaming = data.items[0]?.status?.lifeCycleStatus === "live";
  const isEnd = data.items[0]?.status?.lifeCycleStatus === "complete";
  return { isStreaming, isEnd };
}

// Main function to handle GET request
export async function GET(req: NextRequest) {
  const token = await getToken({ req, secret: process.env.NEXTAUTH_SECRET });

  if (!token || !token.accessToken) {
    console.log("No token or access token found:", token); // Debugging line
    return NextResponse.json({ error: "Unauthorized" }, { status: 401 });
  }

  try {
    console.log("Using access token:", token.accessToken); // Debugging line

    // Fetch live broadcasts
    const items = await fetchLiveBroadcasts(token.accessToken as string);

    // Extract relevant details for each broadcast
    const broadcasts = await Promise.all(
      items.map(async (item: any) => {
        const channelTitle = await fetchChannelDetails(
          item.snippet.channelId,
          token.accessToken as string
        );
        const { isStreaming, isEnd } = await fetchLiveStatus(
          item.id,
          token.accessToken as string
        );

        return {
          id: item.id,
          title: item.snippet.title,
          scheduledStartTime: item.snippet.scheduledStartTime,
          channelTitle,
          iframeSrc: `https://www.youtube.com/embed/${item.id}`,
          isStreaming,
          isEnd,
        };
      })
    );

    return NextResponse.json(broadcasts, { status: 200 });
  } catch (error) {
    console.error("Error fetching data from YouTube API:", error); // Debugging line
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
