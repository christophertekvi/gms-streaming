// src/app/api/stop-stream/route.ts
import { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";


export const POST = async (req, res) => {
  const token = await getToken({ req, secret: process.env.NEXTAUTH_SECRET });

  if (!token || !token.accessToken) {
    return new Response(JSON.stringify({ error: "Unauthorized" }), {
      status: 401,
    });
  }

  const { broadcastId } = await req.json();

  try {
    const response = await fetch(
      `https://www.googleapis.com/youtube/v3/liveBroadcasts/transition?broadcastStatus=complete&id=${broadcastId}&part=id,snippet,contentDetails,status`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token.accessToken}`,
          "Content-Type": "application/json",
        },
      }
    );

    if (!response.ok) {
      const errorText = await response.text();
      return new Response(JSON.stringify({ error: errorText }), {
        status: response.status,
      });
    }

    const data = await response.json();
    const streamStatus = data?.status?.lifeCycleStatus || "completed";
    // alert("Stream has been stopped");
    return new Response(JSON.stringify({ streamStatus }), { status: 200 });
  } catch (error) {
    return new Response(JSON.stringify({ error: error.message }), {
      status: 500,
    });
  }
};
