import { getToken } from "next-auth/jwt";
import pool from "@/utils/db";
import { error } from "console";

export const POST = async (req, res) => {

  const { title, description, scheduledStartTime, privacyStatus } =
    await req.json();

  try {
    const token = await getToken({ req, secret: process.env.NEXTAUTH_SECRET });

    if (!token || !token.accessToken) {
      return new Response(JSON.stringify({ error: "Unauthorized" }), {
        status: 401,
      });
    }
    console.log("Access token retrieved:", token.accessToken);

    // Post data to YouTube API
    const ytResponse = await fetch(
      "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=snippet,status",
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token.accessToken}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          snippet: {
            title,
            description,
            scheduledStartTime,
          },
          status: {
            privacyStatus,
          },
        }),
      }
    );

    const ytData = await ytResponse.json();

    if (!ytResponse.ok) {
      console.error("YouTube API error:", ytData.error.message);
      return new Response(JSON.stringify({status: ytResponse.status, error: ytData.error.message}))
    //   res
    //     .status(ytResponse.status)
    //     .json({ error: ytData.error.message });
    }

    const streamId = ytData.id;
    console.log("YouTube live broadcast created with ID:", streamId);

    // Insert data into MySQL database
    const query = `
      INSERT INTO streaming_schedule (stream_id, title, description, scheduledStartTime, privacyStatus)
      VALUES (?, ?, ?, ?, ?)
    `;

    const [result] = await pool.execute(query, [
      streamId,
      title,
      description,
      scheduledStartTime,
      privacyStatus,
    ]);
    console.log("Data inserted into MySQL, result:", result);

    return new Response(
      JSON.stringify({
        message: "Streaming berhasil dibuat ",
        data: ytData,
        dbResult: result,
      }),
      { status: 200 }
    );
    // return res
    //   .status(200)
    //   .json({ message: "Live broadcast scheduled successfully!", data });
  } catch (error) {
    return new Response(JSON.stringify({ error: error.message }), {
      status: 500,
      // res.status(500).json({ error: error.message });
    });
  }
};
