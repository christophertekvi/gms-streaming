import React from "react";

const YoutubeCard = () => {
  return (
    <div className="YotubeFrame flex flex-col gap-4 bg-[#134074] rounded-xl shadow-2xl p-8">
      <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/XFxhXm8OLuM?si=ldYHMe3Z9g9k9yv8"
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        referrerPolicy="strict-origin-when-cross-origin"
        allowFullScreen
      ></iframe>
      <div className="font-bold text-lg">
        Indonesia | Army of God : Muda & Berdampak - Pdp. Albert Hartanto (Teen
        Online Service) (GMS Church)
      </div>
      <div className="font-medium">11 Mei 2024 | 16:30 WIB | GMS Church</div>
      <button className="text-white bg-green-600 rounded-xl p-8 text-xl font-bold w-full">
        CHECK INPUT
      </button>
    </div>
  );
};

export default YoutubeCard;
