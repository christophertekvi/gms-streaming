/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";

const CompleteStreaming = () => {
  const { data: session, status } = useSession();
  const [completedLivestreams, setCompletedLivestreams] = useState([]);

  const fetchCompletedLivestreams = async () => {
    try {
      const response = await fetch("/api/livestream-schedule/ended", {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        // Filter the schedule to show upcoming streams from today until a week from now
        const now = new Date();
        now.setHours(0, 0, 0, 0); // Start of the day
        const oneWeekFromNow = new Date();
        oneWeekFromNow.setDate(now.getDate() + 7);
        oneWeekFromNow.setHours(23, 59, 59, 999); // End of the last day

        const filteredSchedule = data.filter((item) => {
          const streamDate = new Date(item.scheduledStartTime);
          return streamDate >= now && streamDate <= oneWeekFromNow;
        });
        setCompletedLivestreams(filteredSchedule);
      } else {
        console.error(
          "Failed to fetch completed livestreams:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error fetching completed livestreams:", error);
    }
  };

  useEffect(() => {
    if (status === "authenticated" && session?.accessToken) {
      fetchCompletedLivestreams();
    }
  }, [status, session]);

  const formatDateTime = (dateTimeString) => {
    const date = new Date(dateTimeString);
    const time = date.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
      hour12: false, // Use 24-hour format
    });
    const day = date.getDate();
    const month = date.toLocaleString("en-US", { month: "long" });
    const year = date.getFullYear();
    return `${time} WIB , ${day} ${month} ${year}`;
  };

  if (status === "loading") {
    return <div>Loading...</div>;
  }

  if (status === "unauthenticated") {
    return <div>Please log in to view the schedule.</div>;
  }

  return (
    <div>
      <h2 className="text-white text-2xl font-bold my-4">
        Complete Livestreams
      </h2>
      {completedLivestreams.length > 0 && (
        <div className="grid grid-cols-1 xl:grid-cols-3 md:grid-cols-2 grid-rows-auto gap-4 text-white font-custom">
          {completedLivestreams.map((item) => (
            <div key={item.id} className="bg-[#134074] shadow rounded-lg p-4">
              <iframe
                className="w-full h-80 mt-2"
                src={`https://www.youtube.com/embed/${item.id}`}
                title={item.title}
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
              <h2 className="text-2xl font-bold mt-4 mb-2">{item.title}</h2>
              <p className="text-base">Channel: {item.channelTitle}</p>
              <p className="text-base">
                Scheduled Start Time: {formatDateTime(item.scheduledStartTime)}
              </p>
              <div className="">The stream has ended.</div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default CompleteStreaming;
