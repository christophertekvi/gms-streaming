/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";

const LivestreamSchedule = () => {
  const { data: session, status } = useSession();
  const [schedule, setSchedule] = useState([]);
  const [isProcessing, setIsProcessing] = useState(false);

  const fetchSchedule = async () => {
    try {
      const response = await fetch("/api/livestream-schedule", {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Fetched upcoming schedule:", data); // Debugging line

        // Filter the schedule to show upcoming streams from today until a week from now
        const now = new Date();
        now.setHours(0, 0, 0, 0); // Start of the day
        const oneWeekFromNow = new Date();
        oneWeekFromNow.setDate(now.getDate() + 7);
        oneWeekFromNow.setHours(23, 59, 59, 999); // End of the last day

        const filteredSchedule = data.filter((item) => {
          const streamDate = new Date(item.scheduledStartTime);
          return streamDate >= now && streamDate <= oneWeekFromNow;
        });

        console.log("Filtered schedule: ", filteredSchedule); // Debugging line
        setSchedule(filteredSchedule);
      } else {
        console.error(
          "Failed to fetch upcoming schedule:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error fetching schedule:", error);
    }
  };

  useEffect(() => {
    if (status === "authenticated" && session?.accessToken) {
      fetchSchedule();
    }
  }, [status, session]);

  const formatDateTime = (dateTimeString) => {
    const date = new Date(dateTimeString);
    const time = date.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
      hour12: false, // Use 24-hour format
    });
    const day = date.getDate();
    const month = date.toLocaleString("en-US", { month: "long" });
    const year = date.getFullYear();
    return `${time} WIB , ${day} ${month} ${year}`;
  };

  const handleToggleStream = async (broadcastId, isStreaming) => {
    setIsProcessing(true);
    try {
      const response = await fetch(
        `/api/${isStreaming ? "stop" : "start"}-stream`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session.accessToken}`,
          },
          body: JSON.stringify({ broadcastId }),
        }
      );

      if (response.ok) {
        // Toggle the stream status in the UI
        const data = await response.json();
        if (data.streamStatus === "live") {
          setSchedule((prevSchedule) =>
            prevSchedule.map((item) => {
              if (item.id === broadcastId) {
                return { ...item, isStreaming: true };
              }
              alert("Stream has started");
              return item;
            })
          );
        } else if (data.streamStatus === "completed") {
          setSchedule((prevSchedule) =>
            prevSchedule.map((item) => {
              if (item.id === broadcastId) {
                return { ...item, isStreaming: false };
              }
              alert("Stream has been stopped");
              return item;
            })
          );
        } else {
          alert(data.streamStatus);
        }

        // Refetch the schedule to get updated data
        fetchSchedule();
      } else {
        const errorText = await response.text();
        console.log(response.status + " " + errorText);
        alert("Failed to toggle stream");
      }
    } catch (error) {
      alert("Error toggling stream");
    } finally {
      setIsProcessing(false);
    }
  };

  if (status === "loading") {
    return <div>Loading...</div>;
  }

  if (status === "unauthenticated") {
    return <div>Please log in to view the schedule.</div>;
  }

  return (
    <div>
      <h2 className="text-white text-2xl font-bold mb-4">
        Upcoming Livestreams
      </h2>
      {schedule.length > 0 ? (
        (console.log("Schedule:", schedule), // Debugging line
        (
          <div className="grid grid-cols-1 xl:grid-cols-3 md:grid-cols-2 grid-rows-auto gap-4 text-white font-custom">
            {schedule.map((item) => (
              <div key={item.id} className="bg-[#134074] shadow rounded-lg p-4">
                <iframe
                  className="w-full h-80 mt-2"
                  src={`https://www.youtube.com/embed/${item.id}`}
                  title={item.title}
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
                <h2 className="text-2xl font-bold mt-4 mb-2">{item.title}</h2>
                <p className="text-base">Channel: {item.channelTitle}</p>
                <p className="text-base">
                  Scheduled Start Time:{" "}
                  {formatDateTime(item.scheduledStartTime)}
                </p>
                {item.isEnd ? (
                  <div className="">The stream has ended.</div>
                ) : (
                  <button
                    onClick={() =>
                      handleToggleStream(item.id, item.isStreaming)
                    }
                    className={`${
                      item.isStreaming ? "bg-red-500" : "bg-green-500"
                    } w-full rounded-xl p-4 font-bold mt-4 shadow-md ${
                      item.isStreaming
                        ? "hover:bg-red-700"
                        : "hover:bg-green-700"
                    } ${isProcessing ? "cursor-not-allowed opacity-50" : ""}`}
                    disabled={isProcessing}
                  >
                    {item.isStreaming ? "STOP STREAMING" : "START STREAMING"}
                  </button>
                )}
              </div>
            ))}
          </div>
        ))
      ) : (
        <></>
      )}
    </div>
  );
};

export default LivestreamSchedule;
