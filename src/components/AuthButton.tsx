"use client";
import { signIn } from "next-auth/react";
import Image from "next/image";

export function YoutubeSignIn() {
  const handleClick = () => {
    signIn("google");
  };

  return (
    <button
      onClick={handleClick}
      className="flex items-center font-semibold justify-center h-14 px-6 mt-4 text-lg  transition-colors duration-300 bg-white border-2 border-black text-black rounded-lg focus:shadow-outline hover:bg-slate-200"
    >
      <Image src="/YoutubeLogo.png" alt="Youtube Logo" width={100} height={100} />
      <span className="ml-4">Connnect to YouTube</span>
    </button>
  );
}
